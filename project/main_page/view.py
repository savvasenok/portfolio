from flask import render_template, Blueprint


main_page_blueprint = Blueprint(
    'main_page',
    __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/project.main_page.static'
)

@main_page_blueprint.route('/')
def index():
    return render_template('main_page/index.html')
