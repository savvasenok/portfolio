from flask import render_template, Blueprint


programming_blueprint = Blueprint(
    'programming',
    __name__,
    template_folder='templates',
    static_folder='static'
)

@programming_blueprint.route('/')
def index():
    projects_amount = 0
    projects_titles = []
    projects_texts  = []
    projects_pics   = []
    projects_links  = []

    return render_template(
        'programming/index.html', 
        projects_titles=projects_titles,
        projects_links=projects_links,
        projects_amount=projects_amount,
        projects_texts=projects_texts,
        projects_pics=projects_pics
    )
