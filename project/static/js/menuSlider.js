const headerSlide = ()=>{
	const burger = document.querySelector('.burger');
	const header = document.querySelector('.header-links');
	const headerLinks = document.querySelectorAll('.header-links li');

	burger.addEventListener('click', ()=> {

		// toggle header	
		header.classList.toggle('header-active');

		// burger animation
		burger.classList.toggle('toggle');

		// animate links
		headerLinks.forEach((link, index) => {
			link.style.animation = `headerLinkApear 0.5s ease forwards ${index / 7 + 0.3}s`;
		});

	});
}

headerSlide();
