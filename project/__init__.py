from flask import Flask, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from config import Config


app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)

from project.main_page.view import main_page_blueprint
from project.music.view import music_blueprint
from project.programming.view import programming_blueprint
import project.errors.view as error

app.register_blueprint(main_page_blueprint, url_prefix='/')
app.register_blueprint(music_blueprint, url_prefix='/music')
app.register_blueprint(programming_blueprint, url_prefix='/programming')
app.register_error_handler(404, error.page_not_found)
