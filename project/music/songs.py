from config import basedir
from os import listdir, path


MUSIC_FOLDER_PATH = path.join(basedir, "project", "music", "static", "files")


def get_songs():
	return [path.splitext(song)[0] for song in listdir(MUSIC_FOLDER_PATH)]