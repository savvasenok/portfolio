var isPlaying = false
var isMoving = false
var prevPlayIcon = null
var prevSongID = null
var songID = null
var prevSong = null
var song = null
const playButton = "fa-play"
const pauseButton = "fa-pause"


function play(icon, id) {
	song = document.getElementById(`song${id}`)
	prevSong = document.getElementById(`song${prevSongID}`)

	if (icon.classList.contains(playButton)) {
		icon.classList.remove(playButton)
		icon.classList.add(pauseButton)

		song.play()
		isPlaying = true
	} else if (icon.classList.contains(pauseButton)) {
		icon.classList.remove(pauseButton)
		icon.classList.add(playButton)

		song.pause()
		isPlaying = false
	}

	if ((prevPlayIcon != null) && (prevPlayIcon != icon)) {
		if (prevPlayIcon.classList.contains(pauseButton)) {
			prevPlayIcon.classList.remove(pauseButton)
			prevPlayIcon.classList.add(playButton)
			prevSong.pause()
		}
	}

	prevPlayIcon = icon
	prevSongID = id
	
	song.addEventListener("timeupdate", updateProgress, false)
}

function updateProgress() {
   var timelinePart = document.getElementById(`timeline${prevSongID}`).getBoundingClientRect().width / 100
   var timelineCursor = document.getElementById(`cursor${prevSongID}`)
   var value = 0

   if (song.currentTime > 0) {
      value = song.currentTime / song.duration * 100 * timelinePart
   }

   timelineCursor.style.marginLeft = value + "px"
}

function restartSong(id) {
	var songToRestart = document.getElementById(`song${id}`)
	songToRestart.pause()
	songToRestart.currentTime = 0

	document.getElementById(`cursor${id}`).style.marginLeft = 0

	var mainButton = document.getElementById(`main-button${id}`)
	mainButton.classList.remove(pauseButton)
	mainButton.classList.add(playButton)

}