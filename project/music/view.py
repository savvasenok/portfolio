from flask import render_template, Blueprint
from project.music.songs import get_songs


music_blueprint = Blueprint(
    'music',
    __name__,
    template_folder='templates',
    static_folder='static'
)

@music_blueprint.route('/')
def index():
    songs_list = get_songs()
    return render_template('music/index.html', songs_list=songs_list)
